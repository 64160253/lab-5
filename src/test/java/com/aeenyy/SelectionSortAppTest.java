package com.aeenyy;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SelectionSortAppTest {
  @Test
  public void shouldFindMinIndexTestCase1(){
     int arr[] = {5 , 4 , 3 , 2 , 1};
     int pos = 0;
     int minIndex = SelectionSortApp.FindMinInDex(arr,pos);
     assertEquals(4, minIndex);
  }

  @Test
  public void shouldFindMinIndexTestCase2(){
     int arr[] = {1 , 4 , 3 , 2 , 5};
     int pos = 1;
     int minIndex = SelectionSortApp.FindMinInDex(arr,pos);
     assertEquals(3, minIndex);
  }
  @Test
  public void shouldFindMinIndexTestCase3(){
     int arr[] = {1 , 2 , 3 , 4 , 5};
     int pos = 2;
     int minIndex = SelectionSortApp.FindMinInDex(arr,pos);
     assertEquals(2, minIndex);
  }
  @Test
  public void shouldFindMinIndexTestCase4(){
     int arr[] = {1,1,1,1,1,0,1,1,};
     int pos = 0;
     int minIndex = SelectionSortApp.FindMinInDex(arr,pos);
     assertEquals(5, minIndex);
  }
  @Test
  public void shouldSwapTestCase1(){
     int arr[] = {5,4,3,2,1};
     int expected[] = {1,4,3,2,5};
     int frist = 0;
     int second = 4;
     SelectionSortApp.swap(arr,frist,second);
     assertArrayEquals(expected, arr);
  }
  @Test
  public void shouldSwapTestCase2(){
     int arr[] = {5,4,3,2,1};
     int expected[] = {5,4,3,2,1};
     int frist = 0;
     int second = 0;
     SelectionSortApp.swap(arr,frist,second);
     assertArrayEquals(expected, arr);
  }
  @Test
  public void shouldSelectionTestCase1(){
    int arr[] = {5,4,3,2,1};
    int sortedArr[] = {1,2,3,4,5};
    SelectionSortApp.SelectionSort(arr);
    assertArrayEquals(sortedArr, arr);

  }
  @Test
  public void shouldSelectionTestCase2(){
    int arr[] = {10,9,8,7,6,5,4,3,2,1};
    int sortedArr[] = {1,2,3,4,5,6,7,8,9,10};
    SelectionSortApp.SelectionSort(arr);
    assertArrayEquals(sortedArr, arr);

  }
  @Test
  public void shouldSelectionTestCase3(){
    int arr[] = {6,9,3,7,10,5,4,8,2,1};
    int sortedArr[] = {1,2,3,4,5,6,7,8,9,10};
    SelectionSortApp.SelectionSort(arr);
    assertArrayEquals(sortedArr, arr);
 }
}
